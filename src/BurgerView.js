import React from 'react';
import InnerIngredient from "./Ingredients/InnerIngredients/InnerIngredient";
import BurgerTop from "./Ingredients/Bread/BurgerTop";
import BurgerBottom from "./Ingredients/Bread/BurgerBottom";
import './Burger.css';

const BurgerView = props => {
    return (
        <div className="burgerView">
            <div className="Burger">
                <BurgerTop/>
                {props.ingreds.map(ingred => {
                    console.log(ingred);
                    return <InnerIngredient type={ingred.type} amount={ingred.amount} key={ingred.id} id={ingred.id}/>
                })}
                <BurgerBottom/>
            </div>
        </div>
        )
}

export default BurgerView;