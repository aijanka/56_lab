import React, { Component } from 'react';
import './BurgerBuilder.css';
import BurgerView from '../BurgerView';
import IngredientControl from "../IngredientControl";

class BurgerBuilder extends Component {
    state = {
        ingreds: [
            {type: 'Meat', amount: 2, id: 'm', isZero: false},
            {type: 'Cheese', amount: 2, id: 'c', isZero: false},
            {type: 'Bacon', amount: 1, id: 'b', isZero: false},
            {type: 'Salad', amount: 1, id: 's', isZero: false},
        ]

    };

    makeLess = (type) => {
        let ingreds = [...this.state.ingreds];
        const index = ingreds.findIndex(ingred => ingred.type === type);
        ingreds[index].amount--;

        if(ingreds[index].amount === 0){
            ingreds[index].isZero = true;
        }

        this.setState({ingreds});
    };

    makeMore = (type) => {
        let ingreds = [...this.state.ingreds];
        const index = ingreds.findIndex(ingred => ingred.type === type);
        ingreds[index].amount++;
        ingreds[index].isZero = false;
        this.setState({ingreds});
    };


    render() {
        return (
            <div className="BurgerBuilder">
               <BurgerView ingreds={this.state.ingreds}/>
                <div className="ingredientControls">
                    {this.state.ingreds.map((ingred) => {
                        return <IngredientControl
                            type={ingred.type}
                            amount={ingred.amount}
                            makeLess={() => this.makeLess(ingred.type)}
                            makeMore={() => this.makeMore(ingred.type)}
                            key={ingred.id}
                            isZero={ingred.isZero}
                            />
                    })}
                </div>
            </div>
        );
    }
}

export default BurgerBuilder;