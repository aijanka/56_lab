import React from 'react';

const IngredientControl = props => {
    return (
        <div className="IngredientControl">
            <span className="IngredientControlTitle">{props.type}</span>
            <button className="IngredientControlLess" onClick={props.makeLess} disabled={props.isZero}>Less</button>
            <button className="IngredientControlMore" onClick={props.makeMore} >More</button>
        </div>

    )
};

export default IngredientControl;