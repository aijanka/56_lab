import React from 'react';
import './Bread.css';

const BurgerTop = () => {
    return (
        <div className="BreadTop">
            <div className="Seeds1"></div>
            <div className="Seeds2"></div>
        </div>
    );
};

export default BurgerTop;