import React from 'react';
import './InnerIngredient.css';

const InnerIngredient = props => {
    const ingredArray = [];

    for(let i = 0; i < props.amount; i++){
        let key = props.id;
        console.log(key);
        for(let j = 0; j < i; j++) key += props.id;
        console.log(key)
        ingredArray.push(<div className={props.type} key={key}/>);
    }

    return ingredArray;
}

export default InnerIngredient;